If you want to scrape urls like `https://government.hp.com/resellers_detail.aspx?resellerid=3181` (i.e. without `agencyid` and `state` params), you have to uncomment this line in `reseller.py`:

```
# item = self.normalize_url(item)
```

And comment this line in `settings.py`:

```
COOKIES_ENABLED = False
```

NOTE! For some reasons scraping from `hp.com` is going slow when cookies are enabled. And no, you can't use normalized urls without enabled cookies.
# -*- coding: utf-8 -*-
import re
import scrapy
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode


class ResellersSpider(scrapy.Spider):
    name = 'resellers'
    allowed_domains = ['government.hp.com']
    start_urls = ['https://government.hp.com/resellers.aspx']


    def parse(self, response):
        agencies = response.css('div.dd_formSelection li.dd_item a ::attr(href)').extract()

        for a in agencies:
            yield response.follow(a, callback=self.parse_resellers_list)


    def parse_resellers_list(self, response):
        items_css = 'table#ctl00_ContentPlaceHolder_grdResellersHPE>tr>td>a ::attr(href)'
        items = response.css(items_css).extract()

        for item in items:
            # item = self.normalize_url(item)
            yield response.follow(item, callback=self.parse_reseller)

        link = response.css('a#ctl00_ContentPlaceHolder_grdResellersHPE_ctl01_btnResellerPageForward ::attr(href)').extract_first()
        row_end = response.css('span#ctl00_ContentPlaceHolder_grdResellersHPE_ctl01_labRowEnd ::text').extract_first()
        row_count = response.css('span#ctl00_ContentPlaceHolder_grdResellersHPE_ctl01_labRowCount ::text').extract_first()

        try:
            row_end = int(row_end.strip())
            row_count = int(row_count.strip())
        except:
            row_end = None
            row_count = None

        if all([link, row_end, row_count]) and (row_end < row_count):
            eventTarget = 'ctl00$ContentPlaceHolder$grdResellersHPE$ctl01$btnResellerPageForward'
            eventArgument = ''

            match = re.search("doPostBack\('(.*?)'\s*,\s*'(.*)'\)", link)
            if match:
                eventTarget = match.group(1)
                eventArgument = match.group(2)

            formdata = {'__EVENTTARGET': eventTarget, '__EVENTARGUMENT': eventArgument}
            yield scrapy.FormRequest.from_response(response, formname='aspnetForm', formdata=formdata, method='POST', callback=self.parse_resellers_list)
        # else:
        #     self.logger.warning('Maybe something wrong: link="{}", row_end="{}", row_count="{}"'.format(link, row_end, row_count))


    def parse_reseller(self, response):
        d = {}

        d['url'] = response.url

        title = response.css('div#body h1 ::text').extract_first()
        if title:
            d['title'] = title.strip()

        breadcrumbs = response.css('ul.breadcrumbs li a span.hvr ::text').extract()
        if breadcrumbs:
            d['breadcrumbs'] = ' / '.join(breadcrumbs)

        table = {}
        for tr in response.css('table.rtf_table tr'):
            name = tr.css('td:nth-child(1) span ::text').extract_first()
            value = '\n'.join(filter(None, [t.replace('\n', ' ').strip() for t in tr.css('td:nth-child(2) span ::text').extract() if t]))
            table[name] = value

        if 'Name' in table:
            d['name'] = table['Name']

        if 'Address' in table:
            d['address'] = table['Address']

        if 'Phone' in table:
            d['phone'] = table['Phone']

        if 'Email' in table:
            d['email'] = table['Email']

        if 'Website' in table:
            d['website'] = table['Website']

        if 'Authorizations & Certifications' in table:
            d['certs'] = table['Authorizations & Certifications']

        return d


    def normalize_url(self, url):
        p = list(urlparse(url))
        q = dict(parse_qsl(p[4]))

        if 'resellerid' in q:
            q = {'resellerid': q['resellerid']}
        else:
            self.logger.warning('Unable to find "resellerid" in url. Something may be wrong with <{}>'.format(url))

        p[4] = urlencode(q)
        url = urlunparse(p)

        return url
